This project is a mini pascal compiler. 
The compiler is written in Java its made to run for a mini Pascal language
and convert it into MIPS assembly language.

The compiler has many different parts starting with a scanner.
The Scanner is built using the grammar that was given to us in class.
In total there were six files to build our scanner.

Scanner.jflex which is a Jflex tool that we used to create our Scanner.java file.
Scanner.java which was created through Scanner.Jflex and all modifications for the Scanner.java file 
would be made through the Scanner.Jflex file. 
we have a Token.java file that has the lexeme and the type.
We have a TokenType.java file which was an enum class that had all our token types listed.
We have a LookUpTable.java this file matched each lexeme with its correct Token type. 
and lastly we have the ScannerTest.Java file which is a jUnit test file that we did in order
to test our scanner.

The second part to the compiler is the parser.
The parser consists of two files.
The first file is the Parser.java file.
This file contains the 24 methods found in our grammar packet. 
and a match method that uses the lookahead to see what upcoming token is next.
The next file is the ParserTest.java which is a jUnit test file where i tested
to see if the Parser.java file is working correctly.

Third part to the compiler is the Symboltable.
The symboltable has a SymbolTable.java file. It also holds a Kind.java which is a enum class.
Then we had the jUnit testing in the SymbolTableTest.java file. 