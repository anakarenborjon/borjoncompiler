/**
 * This is a simple example of a jflex lexer definition
 * that is not a standalone application
 */

/* Declarations */

package scanner; 


%%
%public
%class  Scanner   /* Names the produced java file */
%function nextToken /* Renames the yylex() function */
%type   Token      /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}


%{
	LookUpTable LUT = new LookUpTable();
%}


/* Patterns */


E				= [E]
digit 		  = [0-9]
letter        = [A-Za-z]
word          = {letter}+
whitespace    = [ \n\t]
sign 		  = [\+\-]
symbol     	  = (<=|>=|:=|<>|\(|\)|\{|\}|\[|\]|\+|\-|\=|\<|\>|\*|\/|\:|\;|\,|\.)
id 			  = {letter}({letter} | {digit} )* 
integers	  = {digit}{digit}*
decimals      = {integers}(\.){integers}*
exponents     = ({integers}|{decimals}){E}{sign}{integers}
real		  = {decimals} | {exponents}
other		  = .


%%
/* Lexical Rules */

{id}
{

	if (LUT.get(yytext()) != null) return new Token(yytext(), LUT.get(yytext()));
	return new Token(yytext(), TokenType.ID);
}


{integers}
{
	return new Token(yytext(), TokenType.INTEGER);
}
{real}
{
	return new Token(yytext(), TokenType.REAL);
}

{symbol} 
{
	String lexeme = yytext(); 
	Token token = new Token(lexeme, LUT.get(lexeme));
	return token;
}

{word}
{

    String lexeme = yytext();
	if (LUT.get(lexeme) != null)
	{
		Token token = new Token(lexeme, LUT.get(lexeme));
		return token;
	}
	else 
	{
		Token token = new Token(lexeme, TokenType.ID);
	    return token;
	}
		
}
            
{whitespace}  
{  /* Ignore Whitespace */ 
	
}

{other}
{
	String lexeme = yytext();
	return new Token(lexeme, TokenType.ILLEGAL);
}
           