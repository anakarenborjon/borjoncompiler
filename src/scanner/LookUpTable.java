package scanner;
import java.util.HashMap; 

/**
 * 
 * @author Anakaren Borjon
 *
 *This is the look up table of my scanner
 *This contains all the grammar that 
 *was given and is matched with the token type.
 *The look up table then extends TokenType.
 *
 */

@SuppressWarnings("serial")
public class LookUpTable extends HashMap<String, TokenType>

{
	public LookUpTable() 
	{

		this.put("and" , TokenType.AND);
		this.put("array", TokenType.ARRAY);
		this.put("begin", TokenType.BEGIN);
		this.put("number", TokenType.NUM);
		this.put("id", TokenType.ID);
		this.put("end", TokenType.END);
		this.put("while", TokenType.WHILE);
		this.put("mod", TokenType.MOD);
		this.put("integer", TokenType.INTEGER);
		this.put("not", TokenType.NOT);
		this.put("of", TokenType.OF);
		this.put("div", TokenType.DIV);
		this.put("or", TokenType.OR);
		this.put("procedure", TokenType.PROCEDURE);
		this.put("program", TokenType.PROGRAM);
		this.put("real", TokenType.REAL);
		this.put("function", TokenType.FUNCTION);
		this.put("then", TokenType.THEN);
		this.put("if", TokenType.IF);
		this.put("else", TokenType.ELSE);
		this.put("do", TokenType.DO);
		this.put("var", TokenType.VAR);
		this.put("+", TokenType.PLUS);
		this.put("-", TokenType.MINUS);
		this.put(",", TokenType.COMMA);
		this.put(".", TokenType.PERIOD);
		this.put(":", TokenType.COLON);
		this.put("[", TokenType.LBRCKT);
		this.put("]", TokenType.RBRCKT);
		this.put("(", TokenType.LEFTP);
		this.put(")", TokenType.RIGHTP);
		this.put("=", TokenType.EQUALS);
		this.put(">=", TokenType.GREATEQ);
		this.put("<=", TokenType.LESSEQ);
		this.put(">", TokenType.GREATT);
		this.put("<", TokenType.LESST);
		this.put("<>", TokenType.NOTEQUAL);
		this.put("*", TokenType.MULTY);
		this.put("/", TokenType.SLASH);
		this.put(":=", TokenType.ASSIGN);
		this.put(";", TokenType.SEMI);
	}


}

