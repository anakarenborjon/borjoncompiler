package scanner;

import static org.junit.Assert.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;

/**This class is used to test the scanner that was built
 * This reads an input file 
 * and 
 * @return if  
 */
public class ScannerTest 
{
 
	
	@Test
	public void TestScanner() throws IOException
	{
		
		//The token is then declared what token it is expected to be
		//Then the returned token
		
		
		Token expectedT;
		Token returnedT;
		
		//This is used to go into the text file where we get the 
		//variables that will be tested
		
		String filename = System.getProperty("user.dir") + "/input.txt";
		FileInputStream ifs = null;
		try
		{
			ifs = new FileInputStream(filename);


		}
		catch (Exception e ) {e.printStackTrace();}
		InputStreamReader isr = new InputStreamReader(ifs);
		
		Scanner scanner = new Scanner(isr);
		
		expectedT = new Token( "program" , TokenType.PROGRAM );
		returnedT = scanner.nextToken();
		
		//assertEquals hen goes and compares the tokens to see
		//if they are infact the same
		
		assertEquals( expectedT , returnedT );
		
		expectedT = new Token( "merp" , TokenType.ID );
		returnedT = scanner.nextToken();
		assertEquals( expectedT , returnedT );
		
		expectedT = new Token( ";" , TokenType.SEMI );
		returnedT = scanner.nextToken();
		assertEquals( expectedT , returnedT );
		
		
		
	}
	
	
	
	@Test
	/**
	 * The second test is to read the text
	 * and test the lexeme and the yytext
	 * it then compares to see if the lexeme
	 * is equal to the yytext.
	 */
	
	
	
	public void testYytext() throws IOException
	{
		
		//The token is then declared what token it is expected to be
		//Then the returned token
		
		Token expectedT;
		Token returnedT;
		
		String filename = System.getProperty("user.dir") + "/input.txt";
		FileInputStream ifs = null;
		try
		{
			ifs = new FileInputStream(filename);


		}
		catch (Exception e ) {e.printStackTrace();}
		InputStreamReader isr = new InputStreamReader(ifs);
		Scanner scanner = new Scanner(isr);
		
		expectedT = new Token( "program" , TokenType.PROGRAM );
		returnedT = scanner.nextToken();
		assertEquals( expectedT.lexeme , scanner.yytext() );
		
		expectedT = new Token( "merp" , TokenType.ID );
		returnedT = scanner.nextToken();
		assertEquals( expectedT.lexeme , scanner.yytext() );
		
		expectedT = new Token( ";" , TokenType.SEMI );
		returnedT = scanner.nextToken();
		assertEquals( expectedT.lexeme , scanner.yytext() );
		
		
		
	}
	
	
	

}
