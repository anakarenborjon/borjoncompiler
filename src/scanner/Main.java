package scanner;

import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * This is the main class of my scanner
 * 
 * 
 * 
 * 
 */


public class Main 
{

	public static void main(String[] args) 
	{
		String filename = System.getProperty("user.dir") + "/input.txt";
		FileInputStream ifs = null;
		try
		{
			ifs = new FileInputStream(filename);


		}
		catch (Exception e ) {e.printStackTrace();}
		InputStreamReader isr = new InputStreamReader(ifs);
		Scanner scanner = new Scanner(isr);
		Token aToken =null;
		do
		{
			try
			{
				aToken =scanner.nextToken();

			}
			catch (Exception e) {e.printStackTrace();}
			System.out.println("The token was returned " + aToken);
		}
		while(aToken != null);



	}


}
