package scanner;

public class Token 
{

	public String lexeme;
	public TokenType type;


	public Token(String l, TokenType t){
		this.lexeme = l;
		this.type = t;

	}


	@Override
	/**
	 * 
	 *The public boolean equals
	 *was created using the source in Eclipse
	 *to Generate the hashcode and equals.
	 * 
	 */
	

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Token other = (Token) obj;
		if (lexeme == null) {
			if (other.lexeme != null)
				return false;
		} else if (!lexeme.equals(other.lexeme))
			return false;
		if (type != other.type)
			return false;
		return true;
	}


	public TokenType getType() {
		return type;
	}


	public void setType(TokenType type) {
		this.type = type;
	}


	public String getLexeme() {
		return lexeme;
	}


	public void setLexeme(String lexeme) {
		this.lexeme = lexeme;
	}
}