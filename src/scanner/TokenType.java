package scanner;
/**
 * TokenType is a collection of constants 
 * It is part of the scanner package
 * 
 * 
 * 
 */


// The constants are also the same as the ones in the LookUpTable class
// Which extends the TokenType class

public enum TokenType
{
	NUM, ID, PLUS, MINUS, SEMI, ASSIGN, WHILE, PROGRAM,
	AND, ARRAY, BEGIN, DIV, DO, ELSE, END, FUNCTION, IF,
	INTEGER, MOD, NOT, OF, OR, PROCEDURE, REAL, THEN, VAR,
	COLON, PERIOD, COMMA, LBRCKT, RBRCKT, LEFTP, RIGHTP, 
	EQUALS, NOTEQUAL, LESST, GREATT, LESSEQ, GREATEQ,
	MULTY, SLASH, ILLEGAL 
}
