package codegen;

import java.util.Random;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import scanner.TokenType;
import syntaxtree.*;

public class CodeGen {
	
	private static int cRegister = 0;
	private static String hex = new String();
	
	private static String gHex(){
		Random random = new Random();
		int value = random.nextInt();
		return Integer.toHexString(value);
		
	}
	public static void codeFile(String file, ProgramNode root){
		PrintWriter wr;
		try{
			wr = new PrintWriter(new BufferedWriter(new FileWriter(file.substring(0, file.length() -4)+ ".asm")));
			wr.println(codeRoot(root));
		
		}catch 
			(IOException e){
			e.printStackTrace();
		}
	}
	public static String codeRoot(ProgramNode root){
		String c = "";
		
		c += " .data\n\n" + "prompt .asciiz \"Enter Value: \"" + "\n" + "line .asciiz \"\\n\"" + "\n";
		
		if(root.getVariables() != null) {
			c += wCode(root.getVariables());
		}
		if(root.getFunctions() != null) {
			for(SubProgramNode spN : root.getFunctions().getProcs()){
				if(spN.getVariables() != null){
					c += wCode(spN.getParameter());
					c += wCode(spN.getVariables());
				}
			}
			
		}
		c += "\n\n   .text\nmain:\n";
		
		if(root.getFunctions() != null) {
			for(SubProgramNode spN : root.getFunctions().getProcs()){
					c += wCode(spN);
			}
		}
		if(root.getMain() != null){
			c += wCode(root.getMain());
		}
		c += "\njr $ra";
		return c;

	}
	public static String wCode(ParameterNode n){
		String c = "";
		ArrayList<VariableNode> variable = n.getParameter();
		for (VariableNode var : variable){
			c += String.format("%-10    .word 0\n", var.getName() + ":");
		}
		
		return c;
	}
	public static String wCode(SubProgramNode n) {
		String c = "";
		if(n.getFunctions() != null){
			for (SubProgramNode spN : n.getFunctions().getProcs()){
				c += wCode(spN);
			}
		}
		if (n.getMain() != null){
			c += wCode(n.getMain());
		}
		return c;
	}
	public static String wCode(DeclarationsNode n) {
		String c = "";
		ArrayList<VariableNode> v = n.getVariable();
		for(VariableNode vN : v){
			c += String.format("%-10s   .word 0\n", vN.getName() + ":");
		}
		return c;
	}
	public static String wCode(CompoundStatementNode n){
		String c = "";
		ArrayList<StatementNode> st = n.getStatement();
		for (StatementNode sN : st){
			c += wCode(sN);
		}
		return c;
	}
	public static String wCode(StatementNode n){
		
		String c = null;
		
		if(n instanceof AssignmentStatementNode){
			c = wCode((AssignmentStatementNode) n);
		}
		else if( n instanceof ProcedureStatementNode){
			c = wCode((ProcedureStatementNode) n);
		}
		else if( n instanceof CompoundStatementNode){
			c = wCode((CompoundStatementNode) n);
		}
		else if( n instanceof IfStatementNode){
			c = wCode((IfStatementNode) n);
		}
		else if( n instanceof WhileNode){
			c = wCode((WhileNode) n);
		}
		
		return c;
	}
	public static String wCode(AssignmentStatementNode n) {
		String c = "# Assignment-Statment\n";
		ExpressionNode expression = n.getExpression();
		String rRegister = "$t" + cRegister;
		c += wCode(expression, rRegister);
		c += "sw  $t" + cRegister + ", " + n.getLvalue() + "\n";
		return c;
	}
	
	public static String wCode(ProcedureStatementNode n){
		String c = null;
		return("\n");
		
	}
	public static String wCode(IfStatementNode n){
		String c = "\n#  If-Statement\n";
		hex = gHex();
		String reg = "$t" + cRegister;
		c += wCode(n.getTest(), reg);
		c += "beq " + reg + ",  $zero, IfStatmentFailID" + hex + "\n";
		c += wCode(n.getThenStatement());
		c += "j    IfStatementPassID" + hex +"\n";
		c += "IfStatementFailID" + hex + ":\n";
		c += wCode(n.getElseStatement());
		c += "IfStatementPassedID" + hex + ":\n";
		
		return c;
	}
	public static String wCode(WhileNode n){
		String c = "\n#  While-Statement\n";
		hex = gHex();
		String reg = "$t" + cRegister;
		c += wCode(n.getTest(), reg);
		c += "beq " + reg + ",  $zero, WhileCompleteID" + hex + "\n";
		c += wCode(n.getDoStatement());
		c += "j    WhileID" + hex +"\n";
		c += "WhileCompleteID" + hex + ":\n";
		
		
		return c;
	}
	public static String wCode(ReadNode n){
		String c = "\n# Read\n";
		
		c += "li	$v0	4\n";
		c += "la	$a0	prompt\n";
		c += "syscall\n ";
		
		
		Datatype dT = n.getName().getDatatype();
		if(dT == Datatype.INTEGER){
			c += "li	$v0, 5\n";
			c += "syscall\n";
			c += "sw	$v0, " + n.getName();
			
		}
		return c;
	}
	public static String wCode(WriteNode n){
		String c = "\n# Write\n";
		
		if(n.getName() instanceof OperationNode){
			if(n.getName().getDatatype() == Datatype.INTEGER){
			c += wCode(n.getName(), "$s0");
			c += "li	$v0	1\n";
			c += "move	$a0	$s0\n";
			c += "syscall\n ";
		
			}
		}
		else if(n.getName() instanceof VariableNode){
			
			c += wCode(n.getName(), "$s0");
			c += "li	$v0	1\n";
			c += "move	$a0, " + n.getName();
			c += "syscall\n ";
		
			
		}
		c += "#New line\n" + "li	$v0,	4\n"
		+ "la	$a0, newline\n" + "syscall\n";
		
		
		return c;
	}
	public static String wCode(ExpressionNode n, String reg){
		String c = null;
		if(n instanceof OperationNode){
			c = wCode((OperationNode) n, reg); 
		}
		else if(n instanceof ValueNode){
			c = wCode((ValueNode) n, reg); 
		}
		if(n instanceof VariableNode){
			c = wCode((VariableNode) n, reg); 
		}
		
		
		return c;
	}
	public static String wCode(ValueNode n,String result){
		String value = n.getAttribute();
		String c = "addi " + result + " , $zero, " + value + "\n";
		return c;
	}
	public static String wCode(VariableNode n, String result){
		String name = n.getName();
		String c = "lw	" + result + " , " + name + "\n";	
		return c;
		
	}
	public static String wCode(OperationNode oN,String result){
		String c;
		ExpressionNode left = oN.getLeft();
		String lRegister = "$t" + cRegister++;
		c = wCode(left, lRegister);
		ExpressionNode right = oN.getRight();
		String rRegister = "$t" + cRegister++;
		c += wCode(right, rRegister);
		TokenType kindOfOp = oN.getOperation();
		if (kindOfOp == TokenType.PLUS) {
			c += "add     " + result + ",   " + lRegister + ",   " + rRegister + "\n";
		}
		else if (kindOfOp == TokenType.MINUS) {
			c += "sub     " + result + ",   " + lRegister + ",   " + rRegister + "\n";
		}
		else if (kindOfOp == TokenType.MULTY) {
			c += "mult    " + lRegister + ",   " + rRegister + "\n";
			c += "mflo    " + result + "\n";
		}
		else if (kindOfOp == TokenType.SLASH) {
			c += "div     " + lRegister + ",   " + rRegister + "\n";
			c += "mflo    " + result + "\n";
		}
		else if (kindOfOp == TokenType.LESST) {
			c += "slt     " + result + ",   " + lRegister + ",   " + rRegister + "\n";
		}
		else if (kindOfOp == TokenType.GREATT) {
			c += "slt     " + result + ",   " + rRegister + ",   " + lRegister + "\n";
		}
		else if (kindOfOp == TokenType.LESSEQ) {
			c += "addi    " + rRegister + ",   " + rRegister + ",   1\n";
			c += "slt     " + result + ",   " + lRegister + ",   " + rRegister + "\n";
		}
		else if (kindOfOp == TokenType.GREATEQ) {
			c += "addi    " + lRegister + ",   " + lRegister + ",   1\n";
			c += "slt     " + result + ",   " + rRegister + ",   " + lRegister + "\n";
		}
		else if (kindOfOp == TokenType.EQUALS) {
			hex = gHex();
			c += "beq     " + rRegister + ",   " + lRegister + ",   EqualID" + hex + "\n";
			c += "li      " + result + ",   " + "0\n";
			c += "j       EndEqualID" + hex + "\n";
			c += "EqualID" + hex + ":\n";
			c += "li      " + result + ",   " + "1\n";
			c += "EndEqualID" + hex + ":\n";
		}
		else if (kindOfOp == TokenType.NOTEQUAL) {
			hex = gHex();
			c += "beq     " + rRegister + ",   " + lRegister + ",   NotEqualID" + hex + "\n";
			c += "li      " + result + ",   " + "1\n";
			c += "j       EndNotEqualID" + hex + "\n";
			c += "NotEqualID" + hex + ":\n";
			c += "li      " + result + ",   " + "0\n";
			c += "EndNotEqualID" + hex + ":\n";
		}
		cRegister -= 2;
		return c;
	}
	
	
	
	

}
