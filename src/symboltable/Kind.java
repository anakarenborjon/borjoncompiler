package symboltable;
/**
 * 
 * Kind is an enum that
 * can be PROGRAM, VARIABLE, FUNCTION, AND PROCEDURE
 * @author Anakaren
 *
 */


public enum Kind {
	PROGRAM, VARIABLE, FUNCTION, PROCEDURE, ARRAY
	
}
