package symboltable;

import java.util.HashMap;
import java.util.Map.Entry;
import syntaxtree.Datatype;

/**
 * This is my global symbol table
 * it will be used integrated with
 * my parser.
 * 
 * @author Anakaren
 *
 */

public class SymbolTable{
	  
	private HashMap<String, Storage> global;
	
	/**
	 * This is the Constructor for my symbol table.
	 */
	
	public SymbolTable(){
		this.global = new HashMap<String, Storage>(); 
		
	}
	
	/**
	 * This will add the name and kind to our symbol table
	 *@param name
	 * @param kind
	 */
	
	 public void add(String name, Kind kind){
		 this.global.put(name, new Storage(name , kind));
	 }
	 
	 /**
	  * This returns the kind using name as the value.
	  * 
	  * @param name
	  * @return
	  */
	 
	 public Kind getKind(String name) {
		 if (this.global.containsKey(name)) return this.global.get(name).getKind();
		 return null;
		
	 }
	
	 
	 /**
	  * is a boolean function that checks to see
	  * if a ID has a specific kind, and when tested
	  * is checked to see if that kind is matched with that ID
	  * if it matches it returns true if it doesnt match it
	  * returns false. 
	  * @param name
	  * @param kind
	  * @return
	 */
	 public Datatype getDatatype(String lexeme){
		 if(this.global.containsKey(lexeme)) return this.global.get(lexeme).getDatatype();
		 return null;
	 }
	 public void setDatatype(String lexeme, Datatype dT){
		 if (this.global.containsKey(lexeme)) this.global.get(lexeme).setDatatype(dT);
	 }
	 
	 
	 public boolean isKind(String name,Kind kind){
		 if(this.getKind(name) == kind){
			 return true;
		 }
		return false;
	 }
	 
	 /**
	  * Storage is created inside the HashMap
	  * 
	  *
	  */
	 
	 private class Storage{
		 
		private String name;
		 private Kind kind;
		 private Datatype dT;
		 
		 /**
		  *Storage constructor contains the name and the kind 
		  * @param name
		  * @param kind
		  */
		 
		 private Storage(String name, Kind kind){
			 this.name = name;
			 this.kind = kind;
			 this.dT = null;
			 
		 }
		 
		 /**
		  * this getKind is private and the 
		  * only call that can get into the private kind getKind
		  * is the public getKind above. 
		  * @return this.kind
		  */
		 
		 private Kind getKind(){
			 return this.kind;
		 }
		 private Datatype getDatatype(){
			 return dT;
		 }
		 private void setDatatype(Datatype dT){
			 this.dT = dT;
		 }
	}
	 /**
	  * toString is made to print out Symbol table.
	  */
	 
	public String toString(){
		String file = "Key " + "\t\t" +"Value\n";
		for (Entry<String, Storage> entry: this.global.entrySet()){
			file += entry.getKey() + "\t\t"+ entry.getValue().getKind();
		}
		
		return file;
		
	}
	
	

}

