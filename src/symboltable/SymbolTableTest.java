package symboltable;

import static org.junit.Assert.*;
import scanner.Token;
import scanner.TokenType;

import org.junit.Test;
/**
 * SymbolTable jUnit test for kinds.
 * @author Anakaren
 *
 */

public class SymbolTableTest {
	
	/**
	 * 
	 * First test tests the FUNCTION kind
	 * and then compares to see if the two are equal or not
	 */
	
	@Test
	public void test() 
	{
		SymbolTable t = new SymbolTable();
		t.add("merp" , Kind.FUNCTION);
		Kind expected = Kind.FUNCTION;
		Kind returned = t.getKind("merp");
		assertEquals(expected , returned);
		System.out.println("Expected kind is: " + expected +
				" Returned kind is: " + returned);
		
	}
	/**
	 * 
	 * This test tests the two kinds
	 * and returns the kind of the lexeme that we wanted
	 * it to return
	 * and then compares to see if the two are equal or not
	 */
	
	@Test
	public void test2() 
	{
		SymbolTable t = new SymbolTable();
		t.add("merp" , Kind.FUNCTION);
		t.add("tom", Kind.PROGRAM);
		Kind expected = Kind.PROGRAM;
		Kind returned = t.getKind("tom");
		assertEquals(expected , returned);
		System.out.println("Expected kind is: " + expected +
				" Returned kind is: " + returned);
		
	}
	/**
	 * 
	 * This test tests the 3 kinds
	 * and returns the kind of the lexeme that we wanted
	 * it to return
	 * and then compares to see if the two are equal or not
	 */
	
	@Test
	public void test3() 
	{
		SymbolTable t = new SymbolTable();
		t.add("merp" , Kind.FUNCTION);
		t.add("tom", Kind.PROGRAM);
		t.add("joy", Kind.VARIABLE);
		Kind expected = Kind.VARIABLE;
		Kind returned = t.getKind("joy");
		assertEquals(expected , returned);
		System.out.println("Expected kind is: " + expected +
				" Returned kind is: " + returned);
		
	}
	/**
	 * 
	 * This test tests the 4 kinds
	 * and returns the kind of the lexeme that we wanted
	 * it to return
	 * and then compares to see if the two are equal or not
	 */
	
	@Test
	public void test4() 
	{
		SymbolTable t = new SymbolTable();
		t.add("merp" , Kind.FUNCTION);
		t.add("tom", Kind.PROGRAM);
		t.add("joy", Kind.VARIABLE);
		t.add("storm", Kind.PROCEDURE);
		Kind expected = Kind.PROCEDURE;
		Kind returned = t.getKind("storm");
		assertEquals(expected , returned);
		System.out.println("Expected kind is: " + expected +
				" Returned kind is: " + returned);
		
	}
	
	/**
	 * The fifth test sees if the boolean function works
	 * this checks to see if an ID has a specific kind
	 * and checks to see if the kind that was entered is true or false.
	 */

	@Test
	public void test5() 
	{
		SymbolTable t = new SymbolTable();
		t.add("merp" , Kind.FUNCTION);;
		boolean  expected = false;
		boolean returned = t.isKind("merp", Kind.VARIABLE);
		assertEquals(expected , returned);
		System.out.println("Expected kind is: " + expected +
				" Returned kind is: " + returned);
		
	}
	


}
