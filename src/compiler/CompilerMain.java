package compiler;


import parser.Parser;
import analyzer.*;
import syntaxtree.*;

/**
 * Created the Main class for my compiler
 * 
 * 
 * @author Anakaren
 *
 */
public class CompilerMain {

	public static void main(String[] args) {
		
		String file = System.getProperty("user.dir") + "/" + "testCompiler.pas";
		
		file = file.replace("\\", "/");
		Parser parser;
		
		try {		
			parser = new Parser(file, true); 
			ProgramNode pN = parser.program();							
			System.out.println(pN.indentedToString(0));
			
			SemanticAnalysis sA = new SemanticAnalysis(pN);
			sA.Analysis();
			pN = sA.getpN();
			
			

		} catch (Exception e) { e.printStackTrace(); }
		
	
		
	}

	
}


