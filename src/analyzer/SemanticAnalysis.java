package analyzer;
import syntaxtree.*;
import java.util.ArrayList;
import scanner.TokenType;


/**
 * Semantic Analysis goes through and checks to see if there are errors that
 * dont let the file be executed.
 * @author Anakaren
 *
 */

public class SemanticAnalysis {
	private ProgramNode pN;
	private boolean error;
	
	
	public boolean error(){
		return error;
	}
	
	public void error(boolean e){
		this.error = e;
	}
	
	public SemanticAnalysis(ProgramNode pN){
		this.pN = pN;
		this.error = false;
	}
	public ProgramNode getpN(){
		return pN;
	}
	
	public void Analysis(){
		this.confirmType();
		this.confirmDeclared();
		
		if(!this.getpN().getFunctions().getProcs().isEmpty()){
			for (int i = 0; i < this.getpN().getFunctions().getProcs().size(); i++) {
				SemanticAnalysis Als = new SemanticAnalysis(this.getpN().getFunctions().getProcs().get(i));
				Als.Analysis();
				this.getpN().getFunctions().getProcs().set(i, (SubProgramNode) Als.getpN());
			
			}
		
		
		}
	}
	private void confirmType(){
		for (StatementNode sN : pN.getMain().getStatement()){
			StatementType(sN);
		}
	}
	private Datatype StatementType(StatementNode sN){
		if (sN instanceof AssignmentStatementNode) {
			expressionType(((AssignmentStatementNode) sN).getLvalue());
			Datatype dtLeft = ((AssignmentStatementNode) sN).getLvalue().getDatatype();
			Datatype dtExpress = expressionType(((AssignmentStatementNode) sN).getExpression());
			if (dtLeft != dtExpress) {
				if (dtLeft == Datatype.REAL || dtExpress == Datatype.REAL) {
					if (dtLeft == Datatype.REAL) {
						((AssignmentStatementNode) sN).getExpression().setDatatype(Datatype.REAL);
					} else if (dtExpress == Datatype.REAL) {
						System.out.println("Variable " + ((AssignmentStatementNode) sN).getLvalue().getName() + 
								"is being assigned a real. ");
						((AssignmentStatementNode) sN).getLvalue().setDatatype(Datatype.REAL);
						error = true;
						
					}
			
				}
				else if(dtLeft == Datatype.INTEGER || dtExpress == Datatype.INTEGER){
					if(dtLeft == Datatype.INTEGER){
					System.out.println("Variable " + ((AssignmentStatementNode) sN).getLvalue().getName() + 
							"is being assigned as an integer");
					((AssignmentStatementNode) sN).getExpression().setDatatype(Datatype.INTEGER);
						error =true;
					}
					
				}
			
			
			}
		
		
		}
	
		return null;
	}
	private Datatype expressionType(ExpressionNode eN){
		if (eN instanceof ValueNode) {
		} 
		else if (eN instanceof VariableNode) {
		} 
		else if (eN instanceof OperationNode) {
			Datatype dtLeft = expressionType(((OperationNode) eN).getLeft());
			Datatype dtRight = expressionType(((OperationNode) eN).getRight());

			TokenType type = ((OperationNode) eN).getOperation();
			if (type == TokenType.EQUALS || type == TokenType.NOTEQUAL || type == TokenType.LESST
					|| type == TokenType.LESSEQ || type == TokenType.GREATT
					|| type == TokenType.GREATEQ) {
				eN.setDatatype(Datatype.BOOL);
				return Datatype.BOOL;
			}

			if (dtLeft == Datatype.REAL || dtRight == Datatype.REAL) {
				eN.setDatatype(Datatype.REAL);
				return Datatype.REAL;
			} 
			else if (dtLeft == Datatype.INTEGER || dtRight == Datatype.INTEGER) {
				eN.setDatatype(Datatype.INTEGER);
				return Datatype.INTEGER;
			}

		} 
		
		return eN.getDatatype();
		
	}
	private void confirmDeclared(){
		ArrayList<VariableNode> declared = new ArrayList<VariableNode>();
		declared.addAll(pN.getVariables().getVariable());
		
		
		if(this.pN instanceof SubProgramNode){
			declared.addAll(pN.getVariables().getVariable());
			
		}
		ArrayList<VariableNode> used = new ArrayList<VariableNode>();
		for (StatementNode sN : pN.getMain().getStatement()){
			used.addAll(tStatement(sN));
		}
		
		for (VariableNode sN : used) {
			if (!declared.contains(sN)) {
				System.out.println("Variable " + sN.getName());
				error = true;
			}
		}
		
		for (SubProgramNode pN : pN.getFunctions().getProcs()){
			SemanticAnalysis sA = new SemanticAnalysis(pN);
			sA.confirmDeclared();
		}
	}
	private static ArrayList<VariableNode>tStatement(StatementNode sN){
		ArrayList<VariableNode> vN = new ArrayList<VariableNode>();
		
		if (sN instanceof AssignmentStatementNode) {
			vN.add(((AssignmentStatementNode) sN).getLvalue());
			vN.addAll(tExpress(((AssignmentStatementNode) sN).getExpression()));
		} 
		else if (sN instanceof IfStatementNode) {
			vN.addAll(tExpress(((IfStatementNode) sN).getTest()));
			vN.addAll(tStatement(((IfStatementNode) sN).getThenStatement()));
			vN.addAll(tStatement(((IfStatementNode) sN).getElseStatement()));
		} 
		else if (sN instanceof WhileNode) {
			vN.addAll(tExpress(((WhileNode) sN).getTest()));
			vN.addAll(tStatement(((WhileNode) sN).getDoStatement()));
		} 
		else if (sN instanceof ProcedureStatementNode) {
			for (ExpressionNode eN : ((ProcedureStatementNode) sN).getExpression()) {
				vN.addAll(tExpress(eN));
			}
		} else if (sN instanceof ReadNode) {
			vN.addAll(tExpress(((ReadNode) sN).getName()));
		} else if (sN instanceof WriteNode) {
			vN.addAll(tExpress(((WriteNode) sN).getName()));
		}
		return vN;
	}
	public static ArrayList<VariableNode> tExpress(ExpressionNode eN){
		ArrayList<VariableNode> vN = new ArrayList<VariableNode>();
		
		if (eN instanceof ValueNode){	
		}
		else if (eN instanceof VariableNode){
			vN.add((VariableNode) eN);
		}
		else if (eN instanceof OperationNode){
			vN.addAll(tExpress(((OperationNode) eN).getLeft()));
			vN.addAll(tExpress(((OperationNode) eN).getRight()));
		}
		else if (eN instanceof ProcedureNode){
			for(ExpressionNode eNo : ((ProcedureNode) eN).getExpressions() ){
				vN.addAll(tExpress(eNo));
			}
		}
		return vN;
	}
	
	
}
