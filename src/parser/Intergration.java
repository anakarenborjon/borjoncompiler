package parser;

import static org.junit.Assert.*;

import symboltable.Kind;
import symboltable.SymbolTable;

import org.junit.Test;

/**
 * The integreation of the symbol table here is tested with the
 * statement method because it has two possible paths either a variable or a procedure
 * This test goes ahead and tests exactly that.
 * @author Anakaren
 *
 */

public class Intergration {
	
		String proced = "tot (5)";
		String variab = " merp := 5-10 ";
		String vari2 = " hello := 10 -5 ";
	/**
	 * This test case should produce no error because it follows the variable method,
	 * since the kind is variable this should run without any error.
	 */
		
	@Test
	public void test() {
		
		Parser pars = new Parser(variab, false);
		pars.getSymboltable().add("merp", Kind.VARIABLE);
		pars.statement();
		System.out.println(pars.getSymboltable());
		
	
	}
	/**
	 * This should be an error for procedure because it isnt following the
	 * production rules of procedure. 
	 * 
	 */
	
	@Test
	public void test2() {
		
		Parser pars2 = new Parser(variab, false);
		pars2.getSymboltable().add("merp", Kind.PROCEDURE);
		pars2.statement();
		System.out.println(pars2.getSymboltable());
		
	
	}
	/**
	 * This test case tests procedure method. This should not produce any error
	 * because this is an the id tot and the left parenthesis expression list and right parenthesis 
	 * 
	 */
	
	@Test
	public void test3() {
		
		Parser pars3 = new Parser(proced, false);
		pars3.getSymboltable().add("tot", Kind.PROCEDURE);
		pars3.statement();
		System.out.println(pars3.getSymboltable());
		
	
	}
	/**
	 * This test case should not produce an error as well since this one also follows
	 * variable method for what it is, and since the kind is variable no error should be produced.
	 * 
	 */
	
	@Test
	public void test4() {
		Parser pars4 = new Parser(vari2, false);
		pars4.getSymboltable().add("hello", Kind.VARIABLE);
		pars4.statement();
		System.out.println(pars4.getSymboltable());
		
	
	}
	
	
	
	
	

}
