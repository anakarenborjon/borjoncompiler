/**
 * Parser Package is were
 * the compilers parser is kept. 
 * 
 *  
 */

package parser;

import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;

import scanner.Token;
import scanner.TokenType;
import scanner.Scanner; 
import symboltable.SymbolTable;
import symboltable.Kind;
import syntaxtree.*;



/**
 * The parser recognizes whether an input string of tokens
 * is an expression.
 * If no error returns that means that 
 * it is an acceptable expression. 
 * 
 * 
 * @author Anakaren Borjon
 *
 */

public class Parser
{
	
	//Instance Variables
	
	private  Token lookahead;
	private Scanner scanner; 
	private SymbolTable symboltable;
	
	
	//Constructors for the parser
	
	 public Parser( String text, boolean isFile)
	 {
		 symboltable = new SymbolTable();
	        if( isFile){
	        	FileInputStream fis = null;
	        try {
	            fis = new FileInputStream(text);
	            System.out.println(text);
	        } catch (Exception e) {e.printStackTrace();
	        }
	        InputStreamReader isr = new InputStreamReader( fis);
	        scanner = new Scanner( isr);
	                 
	        }
	        else{
	            scanner = new Scanner( new StringReader( text));
	        }
	        try{
	            lookahead = scanner.nextToken();
	        } catch (IOException e) {  
	        	error( "Scan error");
	        
	        }
	    }
	
	// 24 methods found in our grammar packet
	 // each has a set of rules that are being followed
	
	 /**
	  * program production rules
	  * program id ;
	  * declarations
	  * subprogram_declarations
	  * compound_statement
	  * .
	  * 
	  */
	
	public ProgramNode program()
	{
		match(TokenType.PROGRAM);
		ProgramNode pN = new ProgramNode(lookahead.getLexeme());
		this.symboltable.add(lookahead.getLexeme(), Kind.PROGRAM);
		match(TokenType.ID);
		match(TokenType.SEMI);
		pN.setVariables(declarations(new DeclarationsNode())); 
		pN.setFunctions(subprogram_declarations(new SubProgramDeclarationsNode()));
		pN.setMain(compound_statement());
		match(TokenType.PERIOD);
		return pN;
		
		
	}
	
	/**
	 * Identifier_list production rules
	 * id |
	 * id, identifier_list
	 */
	
	public ArrayList<VariableNode> identifier_list(ArrayList<VariableNode> vN)
	{
		vN.add(new VariableNode(lookahead.getLexeme()));
		this.symboltable.add(lookahead.getLexeme(), Kind.VARIABLE);
		match(TokenType.ID);
		if (lookahead != null && lookahead.getType() == TokenType.COMMA){
			match(TokenType.COMMA);
			vN = identifier_list(vN);
		}
		return vN;
		
		//No else needed because there is a lambda here
		
		
		
	}
	
	/**
	 * declarations production rules
	 * var identifier_list : type ; declarations |
	 * lambda
	 * 
	 */
	
	public DeclarationsNode declarations(DeclarationsNode dN){
		ArrayList<VariableNode> vN = new ArrayList<>();
		Datatype dT = null;
		
		if(lookahead != null && lookahead.getType() == TokenType.VAR){
			match(TokenType.VAR);
			vN = identifier_list(new ArrayList<VariableNode>());
			match(TokenType.COLON);
			dT = type();
			match(TokenType.SEMI);
			dN = declarations(dN);
		}

		//No else needed because lambda is here
		for(VariableNode vNode : vN){
			vNode.setDatatype(dT);
			dN.addVariable(vNode);
			symboltable.setDatatype(vNode.getName(), dT);
		}
		
		
		return dN;
		
		
		
		
	}
	
	/**
	 * type production rules
	 * standard_type |
	 * array [ num : num ] of standar_type
	 */
	
	public Datatype type(){
		Datatype dT = null;
		if(lookahead != null && lookahead.getType() == TokenType.ARRAY){
			match(TokenType.ARRAY);
			match(TokenType.LBRCKT);
			if (lookahead.getType() == TokenType.INTEGER){ 
				match(TokenType.INTEGER); 
			}
			else { 
				match(TokenType.REAL); 
			}
			match(TokenType.COLON);
			if (lookahead.getType() == TokenType.INTEGER){ 
				match(TokenType.INTEGER); 
			}
			else { 
				match(TokenType.REAL); 
			}
			match(TokenType.RBRCKT);
			match(TokenType.OF);
			
		}
		dT = standard_type();
		return dT;
		
	}
	
	/**
	 * standard_type production rules
	 * integer |
	 * real
	 * 
	 */
	
	public Datatype standard_type(){
		if(lookahead != null && lookahead.getType() == TokenType.INTEGER){
			match(TokenType.INTEGER);
			return Datatype.INTEGER;
			
		}
		else if(lookahead != null && lookahead.getType() == TokenType.REAL){
			match(TokenType.REAL);
			return Datatype.REAL;
		}
		else {
			error("standard_type()");
		
			return null;
		}
	}
	
	/**
	 * subprogram_declarations production rules
	 * subprogram_declaration ; 
	 * subprogram_declarations |
	 * lambda
	 * 
	 */
	
	public SubProgramDeclarationsNode subprogram_declarations(SubProgramDeclarationsNode spdNode){
		
		
		if(lookahead != null && lookahead.getType() == TokenType.FUNCTION || lookahead.getType() == TokenType.PROCEDURE){
			spdNode.addSubProgramDeclaration(subprogram_declaration());
			match(TokenType.SEMI);
			spdNode = subprogram_declarations(spdNode);
		}
		//Else Lambda no need for else statement
		return spdNode;
		
		
		
		
	}
	
	/**
	 * subprogram_declaration production rules
	 * subprogram_head
	 * declarations
	 * subprogram_declarations
	 * compound_statement
	 * 
	 */
	
	public SubProgramNode subprogram_declaration(){
		
		SubProgramNode spN = subprogram_head();
		spN.setVariables(declarations( new DeclarationsNode()));
		spN.setFunctions(subprogram_declarations(new SubProgramDeclarationsNode()));
		spN.setMain(compound_statement());
		
		return spN;
		
		
	}
	
	/**
	 * subprogram_head production rules
	 * function id arguments : standar_type ; |
	 * procedure id arguments ;
	 * 
	 */
	
	public SubProgramNode subprogram_head(){
		SubProgramNode spN = null;
		
		if(lookahead != null && lookahead.getType() == TokenType.FUNCTION){
			match(TokenType.FUNCTION);
			spN = new SubProgramNode(lookahead.getLexeme());
			spN.setType(SubpT.FUNCTION);
			this.symboltable.add(lookahead.getLexeme(), Kind.FUNCTION);
			match(TokenType.ID);
			
			ArrayList<VariableNode> vNode = arguments(new ArrayList<VariableNode>()); 
			ParameterNode paNode = new ParameterNode();
			for (VariableNode vN : vNode){
				paNode.addVariable(vN);
			}
			
			spN.setParameter(paNode);
			match(TokenType.COLON);
			Datatype dT = standard_type();
			this.symboltable.setDatatype(spN.getName(), dT);
			match(TokenType.SEMI);
		}
		else if(lookahead != null && lookahead.getType() == TokenType.PROCEDURE){
			match(TokenType.PROCEDURE);
			spN = new SubProgramNode(lookahead.getLexeme());
			spN.setType(SubpT.PROCEDURE);
			this.symboltable.add(lookahead.getLexeme(), Kind.PROCEDURE);
			match(TokenType.ID);
			
			ArrayList<VariableNode> vNode = arguments(new ArrayList<VariableNode>());
			ParameterNode paNode = new ParameterNode();
			for (VariableNode vN : vNode){
				paNode.addVariable(vN);
			}
			spN.setParameter(paNode);
			match(TokenType.SEMI);
		}
		else{
			error("subprogram_head()");
		}
		
		return spN;
		
		
	}
	
	/**
	 * argument production rule
	 * ( parameter_list ) |
	 * lambda
	 * 
	 */
	
	public ArrayList<VariableNode> arguments(ArrayList<VariableNode> vNode){
		if(lookahead != null && lookahead.getType() == TokenType.LEFTP){
			match(TokenType.LEFTP);
			vNode = parameter_list(vNode);
			match(TokenType.RIGHTP);
		
		}
		//No else required lambda
		return vNode;
	}
	
	/**
	 * parameter_list production rules
	 * identifier_list : type |
	 * identifier_list : type ; parameter_list
	 * 
	 * 
	 */
	
	public ArrayList<VariableNode> parameter_list(ArrayList<VariableNode> vN){
		
		ArrayList<VariableNode> vNodes = identifier_list(new ArrayList<VariableNode>());
		match(TokenType.COLON);
		Datatype dT = type();
		if(lookahead != null && lookahead.getType() == TokenType.SEMI){
			match(TokenType.SEMI);
			vNodes = parameter_list(vNodes);
		}
		for (VariableNode vNode : vNodes){
			vNode.setDatatype(dT);
			vNodes.add(vNode);
			symboltable.setDatatype(vNode.getName(), dT);
		}
		
		return vN;
		
		//Populate arguments
		
	}
	
	/**
	 * compound_statement production rules
	 * begin optional_statements end
	 */
	
	public CompoundStatementNode compound_statement(){
		
		CompoundStatementNode csNode = new CompoundStatementNode();
		match(TokenType.BEGIN);
		csNode = optional_statement(csNode);
		match(TokenType.END);
		return csNode;
		
		
	}
	
	/**
	 * 
	 * optional_statement production rules
	 * statement_list |
	 * lambda
	 * 
	 */
	
	public CompoundStatementNode optional_statement(CompoundStatementNode csNode){
	
		if(lookahead != null && lookahead.getType() == TokenType.ID || lookahead.getType() == TokenType.IF || lookahead.getType() == TokenType.WHILE ||
				lookahead.getType() == TokenType.BEGIN)
		{
			csNode = statement_list(csNode);
		}
		//no else required lambda
		return csNode;
		
		
	}
	
	/**
	 * 
	 * statement_list production rules
	 * statement |
	 * statement ; statement_list
	 */
	
	public CompoundStatementNode statement_list(CompoundStatementNode csNode){
		csNode.addStatement(statement());
		
		if(lookahead != null && lookahead.getType() == TokenType.SEMI){
			match(TokenType.SEMI);
			csNode = statement_list(csNode);
		}
		return csNode;
		
		
		
	}
	
	/**
	 * statement production rules
	 * variable assignop expression |
	 * procedure_statement |
	 * compound_statement |
	 * if expression then statement else statement |
	 * while expression do statement |
	 * read (id)
	 * write (expression)
	 * 
	 */
	
	public StatementNode statement(){
		if(lookahead != null && lookahead.getType() == TokenType.ID){
			if(symboltable.isKind(lookahead.getLexeme(), Kind.VARIABLE)){
				AssignmentStatementNode asN = new AssignmentStatementNode();
				asN.setLvalue(variable());
				match(TokenType.ASSIGN);
				asN.setExpression(expression());
				return asN;
			}
		}
		else if(lookahead != null && lookahead.getType() == TokenType.ID && this.symboltable.isKind(lookahead.getLexeme(), Kind.PROCEDURE)){
			return procedure_statement();
		}
		else if(lookahead != null && lookahead.getType() == TokenType.BEGIN){
			return compound_statement();
			
		}
		else if(lookahead != null && lookahead.getType() == TokenType.IF){
			IfStatementNode iN = new IfStatementNode();
			match(TokenType.IF);
			iN.setTest(expression());
			match(TokenType.THEN);
			iN.setThenStatement(statement());
			match(TokenType.ELSE);
			iN.setElseStatement(statement());
			return iN;
		}
		else if(lookahead != null && lookahead.getType() == TokenType.WHILE){
			WhileNode wN = new WhileNode();
			match(TokenType.WHILE);
			wN.setTest(expression());
			match(TokenType.DO);
			wN.setDoStatement(statement());
			return wN;
		}
		else {
			error("statment()");
		}
		return null;
		
		
		
		
	}
	
	/**
	 * variable production rules
	 * id |
	 * id [ expression ]
	 * 
	 */
	
	
	public VariableNode variable(){
		String name = lookahead.getLexeme();
		match(TokenType.ID);
		if(lookahead != null && lookahead.getType() == TokenType.LBRCKT){
			ArrayNode aN = new ArrayNode(name);
			match(TokenType.LBRCKT);
			aN.setExpression(expression());
			match(TokenType.RBRCKT);
		}
		
		VariableNode vNode = new VariableNode(name);
		Datatype dT = this.symboltable.getDatatype(name);
		vNode.setDatatype(dT);
		return vNode;
	}
	
	/**
	 * 
	 * procedure_statement production rules
	 * id |
	 * id ( expression_list )
	 * 
	 */
	
	public ProcedureStatementNode procedure_statement(){
		ProcedureStatementNode pN = new ProcedureStatementNode(lookahead.getLexeme());
		match(TokenType.ID);
		if(lookahead != null && lookahead.getType() == TokenType.LEFTP){
			match(TokenType.LEFTP);
			ArrayList<ExpressionNode> eN = expression_list(new ArrayList<ExpressionNode>());
			match(TokenType.RIGHTP);
			for (ExpressionNode eNodes : eN) {
				pN.addExpression((eNodes));
			}
		}
		return pN;
		
	}
	
	/**
	 * expression_list production rules
	 * expression |
	 * expression , expression_list
	 * 
	 */
	
	public ArrayList<ExpressionNode> expression_list(ArrayList<ExpressionNode> eN){
		eN.add(expression());
		if(lookahead != null && lookahead.getType() == TokenType.COMMA){
			match(TokenType.COMMA);
			eN = expression_list(eN);
		}
		return eN;
		
	}
	
	/**
	 * 
	 * expression production rules
	 * simple_expression |
	 * simple_expression relop simple_expression
	 */
	
	
	public ExpressionNode expression(){
		
		ExpressionNode eN = simple_expression();
		if(lookahead != null && isRelop(lookahead)){
			OperationNode oN = new OperationNode(relop());
			oN.setLeft(eN);
			oN.setRight(simple_expression());
			return oN;
		}
		return eN;
	}
	
	/**
	 * simple_expression production rules
	 * term simple_part |
	 * sign term simple_part
	 * 
	 */
	
	public ExpressionNode simple_expression(){
		ExpressionNode eN = null;
		OperationNode oN = null;
		if(lookahead != null && lookahead.getType() == TokenType.MINUS || lookahead.getType() == TokenType.PLUS){
			oN = sign();
		}
		if (oN != null) {
			oN.setRight(term());
			eN = oN;
		} else {
			eN = term();
		}
		eN = simple_part(eN);
		return eN;
	}
	
	/**
	 * simple_part production rules
	 * addop term simple_part
	 * lambda
	 */
	
	public ExpressionNode simple_part(ExpressionNode eN){
		OperationNode oN = null;
		if(lookahead != null && isAddop(lookahead)){
			oN = new OperationNode(addop());
			oN.setLeft(eN);
			oN.setRight(term());
			return simple_part(oN);
		}
		// lambda
		return eN;
		
		
	}
	
	/**
	 * term production rules
	 * factor term_part
	 * 
	 */
	
	public ExpressionNode term(){
		ExpressionNode eN = null;
		eN = factor();
		eN = term_part(eN);
		return eN;
		
	}
	
	/**
	 * term_part production rules
	 * mulop factor term_part |
	 * lambda
	 */
	
	public ExpressionNode term_part(ExpressionNode eN){
		OperationNode oN = null;
		if(lookahead != null && isMulop(lookahead)){
			oN = new OperationNode(mulop());
			oN.setLeft(eN);
			oN.setRight(factor());
			return term_part(oN);
			
		}
		return eN;
		//Else lambda no need for else statement
		
		
	}
	
	/**
	 * factor production rules 
	 * id |
	 * id [ expression ] |
	 * id ( expression_list ) |
	 * num |
	 * ( expression ) |
	 * not factor
	 */
	
	public ExpressionNode factor(){
		if(lookahead != null && lookahead.getType() == TokenType.ID){
			String name = lookahead.getLexeme();
			match(TokenType.ID);
			if(lookahead != null && lookahead.getType() == TokenType.LBRCKT){
				match(TokenType.LBRCKT);
				ArrayNode eN = new ArrayNode(name);
				eN.setExpression(expression());
				match(TokenType.RBRCKT);
				return eN;
			}
			else if(lookahead != null && lookahead.getType() == TokenType.LEFTP){
				match(TokenType.LEFTP);
				ProcedureNode pN = new ProcedureNode(name);
				pN.setDatatype(this.symboltable.getDatatype(name));
				ArrayList<ExpressionNode> eNo = expression_list(new ArrayList<ExpressionNode>());
				match(TokenType.RIGHTP);
				for (ExpressionNode eN : eNo) {
					pN.addExpression(eN);
				}
				return pN;
			}
			VariableNode vNode = new VariableNode(name);
			vNode.setDatatype(symboltable.getDatatype(name));
			return vNode;
			
		}
		else if(lookahead != null && lookahead.getType() == TokenType.INTEGER || lookahead.getType() == TokenType.REAL){
			ValueNode vN = new ValueNode(lookahead.getLexeme());
			if(lookahead.getType() == TokenType.INTEGER){
					vN.setDatatype(Datatype.INTEGER);
					match(TokenType.INTEGER);
			}
			else if (lookahead.getType() == TokenType.REAL){
				vN.setDatatype(Datatype.REAL);
				match(TokenType.REAL);
			}
			return vN;
			
		}
		else if(lookahead != null && lookahead.getType() == TokenType.LEFTP){
			match(TokenType.LEFTP);
			ExpressionNode eN = expression();
			match(TokenType.RIGHTP);
			return eN;
		}
		else if(lookahead != null && lookahead.getType() == TokenType.NOT){
			match(TokenType.NOT);
			return factor();
			
		}
		else {
			error("factor()");
			return null;
		}
		
		
		
	}
	
	/**
	 * sign production rules 
	 * + |
	 * -
	 */
	
	public OperationNode sign(){
		if(lookahead != null && lookahead.getType() == TokenType.PLUS){
			match(TokenType.PLUS);
			OperationNode oN = new OperationNode(TokenType.PLUS);
			oN.setLeft(new ValueNode("0"));
			return oN;
		}
		else if(lookahead != null && lookahead.getType() == TokenType.MINUS){
			match(TokenType.MINUS);
			OperationNode oN = new OperationNode(TokenType.MINUS);
			oN.setLeft(new ValueNode("0"));
			return oN;
		}
		else {
			error("sign()");
		}
		return null;
		
	}
	
	/**
	 * this is the getter to the private symboltable 
	 * this is the only way to get the symboltable. 
	 * @return this.symboltable
	 */
	
	public SymbolTable getSymboltable() {
		return this.symboltable;
	}

	public void setSymboltable(SymbolTable symboltable) {
		this.symboltable = symboltable;
	}

	/**
	*This boolean checks to see if isRelop look ahead token
	*is any of the following tokens EQUALS, NOTEQUAL, LESST, GREATT, GREATEQ and LESSEQ
	*if it is any it returns true if not then it is false.
	**/
	
	public boolean isRelop(Token token){
		TokenType tokentype = token.getType();
		
		if(lookahead.getType() == TokenType.EQUALS || lookahead.getType() == TokenType.NOTEQUAL || lookahead.getType() == TokenType.LESST || lookahead.getType() == TokenType.GREATT ||
				lookahead.getType() == TokenType.GREATEQ || lookahead.getType() == TokenType.LESSEQ){
			
			return true;
		}
		
		
		return false;	
	}
	
	/**
	*This boolean checks to see if isAddop look ahead token
	*is any of the following tokens PLUS, MINUS, and OR
	*if it is any it returns true if not then it is false.
	**/
	
	public boolean isAddop(Token token){
		TokenType tokentype = token.getType();
		if(lookahead.getType() == TokenType.PLUS || lookahead.getType() == TokenType.MINUS || lookahead.getType() == TokenType.OR){
			return true;
			}
		
	    return false;	
	}
	
	/**
	*This boolean checks to see if isMulop look ahead token
	*is any of the following tokens AND, DIV, SLASH, MOD, MULTY
	*if it is any it returns true if not then it is false.
	**/
	
	public boolean isMulop(Token token){
		TokenType tokentype = token.getType();
		if(lookahead.getType() == TokenType.AND || lookahead.getType() == TokenType.DIV || lookahead.getType() == TokenType.MOD 
				|| lookahead.getType() == TokenType.SLASH || lookahead.getType() == TokenType.MULTY){
			return true;
			}
		return false;	
	}
	
	/**
	*the mulop matches one of these tokentype that
	*were provided in the grammar packet
	**/
	
	public TokenType mulop(){
		switch(lookahead.getType())
		{
		case AND:
		
			match(TokenType.AND);
			return TokenType.AND;
		
		case DIV:
		
			match(TokenType.DIV);
			return TokenType.DIV;
		
		case MOD:
		
			match(TokenType.MOD);
			return TokenType.MOD;
		
		case SLASH:
		
			match(TokenType.SLASH);
			return TokenType.SLASH;
		
		case MULTY:
		
			match(TokenType.MULTY);
			return TokenType.MULTY;
		
		default:
		
			error("mulop()");
			return TokenType.ILLEGAL;
		}	
		
		
		}
	
	
	/**
	*the addop matches one of these tokentype that
	*were provided in the grammaer packet
	* which are +, -, and OR
	**/
	
	public TokenType addop(){
		switch(lookahead.getType())
		{
		case PLUS:
		
			match(TokenType.PLUS);
			return TokenType.PLUS;
		
		case MINUS:
		
			match(TokenType.MINUS);
			return TokenType.MINUS;
		
		case OR:
		
			match(TokenType.OR);
			return TokenType.OR;
		
		default:
		
			error("addop()");
			return TokenType.ILLEGAL;
		}
	}
	/**
	*the relop matches one of these tokentype that
	*were provided in the grammaer packet
	* =, <>, >=, <=, >, <,  
	**/
	public TokenType relop(){
		switch(lookahead.getType())
		{
		case EQUALS:
		
			match(TokenType.EQUALS);
			return TokenType.EQUALS;
		
		case NOTEQUAL:
		
			match(TokenType.NOTEQUAL);
			return TokenType.NOTEQUAL;
		
		case LESST:
		
			match(TokenType.LESST);
			return TokenType.LESST;
		
		case GREATT:
		
			match(TokenType.GREATT);
			return TokenType.GREATT;
		
		case GREATEQ:
		
			match(TokenType.GREATEQ);
			return TokenType.GREATEQ;
		
		case LESSEQ:
		
			match(TokenType.LESSEQ);
			return TokenType.LESSEQ;

		default:
		
			error("relop()");
			return TokenType.ILLEGAL;
		
		
		}
	}
	
	/**The match token is what compares
	//the expected token to one of 
	//The token types that were created in our scanner
	**/
	
	
	public void match(TokenType expected){
		
		
		if(this.lookahead.getType() == expected ){
			try {
				this.lookahead = scanner.nextToken(); 
				if (this.lookahead == null) {
					this.lookahead = new Token("End of file ", null); 
				}
				
				
				
			}
			catch (IOException c) {
				error("Scanner exception");
			}
			
		}
		else {
			error("match of " + expected + " found " + this.lookahead.getType() + " instead. ");
		}
		
		
	}
	
	/**The error method
	*Prints out the error message. 
	**/
	public void error(String message){
		System.out.println("Error "+ message);
                
	}
		
		
		
	

	
}

