package parser;

import static org.junit.Assert.*;

import syntaxtree.*;
import org.junit.Test;

import scanner.TokenType;
import symboltable.Kind;



/**
 * Unit Testing for the SyntaxTree
 * @author Anakaren
 *
 */
public class TreeTest {
	
	/**
	 * Created a Manual Program node 
	 * that includes Factor, Simple expression, statement
	 * , subprogram declarations, declarations,
	 * 
	 * 
	 */
	
	@Test
	public void test() {
		ProgramNode pNode = new ProgramNode("merp");
		pNode.setVariables(new DeclarationsNode());
		pNode.setFunctions(new SubProgramDeclarationsNode());
		pNode.setMain(new CompoundStatementNode());
		String expected = pNode.indentedToString(0);
		String test = "program merp; begin end .";
		Parser par = new Parser(test, false);
		String actual = par.program().indentedToString(0);
		assertEquals(expected, actual);
	}
	/**
	 * Testing the Factor Function
	 */
	
	@Test
	public void FactorT(){
		ValueNode vN = new ValueNode("8");
		String expected = vN.indentedToString(0);
		String test = "8";
		Parser par = new Parser(test, false);
		String actual = par.factor().indentedToString(0);
		assertEquals(expected,actual);
	}
	
	@Test
	public void FactorT2(){
		ValueNode vN = new ValueNode("8");
		String expected = vN.indentedToString(0);
		String test = "8";
		Parser par = new Parser(test, false);
		String actual = par.factor().indentedToString(0);
		assertEquals(expected,actual);
	}
	/**
	 * Testing declarations function
	 */
	
	@Test
	public void Declarations(){
		DeclarationsNode dN = new DeclarationsNode();
		dN.addVariable(new VariableNode("merp"));
		dN.addVariable(new VariableNode("slurp"));
		dN.addVariable(new VariableNode("lurk"));
		String expected = dN.indentedToString(0);
		String test = "var merp, slurp, lurk : integer;";
		Parser par = new Parser(test, false);
		String actual = par.declarations(new DeclarationsNode()).indentedToString(0);
		assertEquals(expected,actual);
	}
		/**
		 * Testing Simple_expression
		 */
		@Test
		public void simpleExpressionT(){
			OperationNode MinusN = new OperationNode(TokenType.MINUS);
			MinusN.setLeft(new ValueNode("10"));
			MinusN.setRight(new ValueNode ("5"));
			String expected = MinusN.indentedToString(0);
			String test = "10 - 5";
			Parser par = new Parser(test, false);
			String actual = par.simple_expression().indentedToString(0);
			assertEquals(expected, actual);
					
			
		}
		/**
		 * Testing the if statement
		 */
		@Test
		public void statementT(){
			IfStatementNode iN = new IfStatementNode();
			OperationNode oN = new OperationNode(TokenType.EQUALS);
			oN.setLeft(new VariableNode("x"));
			oN.setRight(new ValueNode("3"));
			
			AssignmentStatementNode aN = new AssignmentStatementNode();
			aN.setLvalue(new VariableNode("x"));
			aN.setExpression(new ValueNode("5"));
			AssignmentStatementNode aN_2 = new AssignmentStatementNode();
			aN_2.setLvalue(new VariableNode("x"));
			aN_2.setExpression(new ValueNode("3"));
			
			iN.setTest(oN);
			iN.setThenStatement(aN);
			iN.setElseStatement(aN_2);
			String expected = iN.indentedToString(0);
			
			String test = "if x = 3 then x := 5 else x := 3";
			Parser par = new Parser(test, false);
			par.getSymboltable().add("x", Kind.VARIABLE);
			String actual = par.statement().indentedToString(0);
			assertEquals(expected, actual);
			
		}
		/**
		 * Tests the While statement 
		 */
		
		@Test
		public void StatementTestW(){
			WhileNode wN = new WhileNode();
			
			OperationNode oN = new OperationNode(TokenType.LESST);
			oN.setLeft(new VariableNode("merp"));
			oN.setRight(new ValueNode("5"));
			
			AssignmentStatementNode Merpp = new AssignmentStatementNode();
			OperationNode oN2 = new OperationNode(TokenType.PLUS);
			oN2.setLeft(new VariableNode("merp"));
			oN2.setRight(new ValueNode("3"));
			
			Merpp.setLvalue(new VariableNode("merp"));
			Merpp.setExpression(oN2);
			
			wN.setTest(oN);
			wN.setDoStatement((Merpp));
			
			String expected = wN.indentedToString(0);
			String test = "while merp < 5 do merp := merp + 3";
			Parser par = new Parser(test, false);
			par.getSymboltable().add("merp", Kind.VARIABLE);
			String actual = par.statement().indentedToString(0);
			assertEquals(expected, actual);
			
			
			
			
		}
		
		@Test
		public void SubPD(){
			
			SubProgramDeclarationsNode  spdNode = new SubProgramDeclarationsNode();
			SubProgramNode spNode = new SubProgramNode("foo");
			spNode.setType(SubpT.FUNCTION);
			

			String expected = spdNode.indentedToString(0);
			
			String test = "function foo (fi, fum : integer) : integer ; begin fi := 3 end";
			Parser par = new Parser(test, false);
			String actual = par.subprogram_declaration().indentedToString(0);
			//assertEquals(expected, actual);
			System.out.println(actual);
			assertEquals(expected, actual);
		}

}
	
	
	
