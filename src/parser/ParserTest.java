package parser;

import static org.junit.Assert.*;

import org.junit.Test;

import syntaxtree.DeclarationsNode;

/**
 * This is the Unit testing 
 * for the parser we are supposed to do
 * multiple tests for the following methods
 *  program, declarations,
 * subprogram_declaration, statement, simple_expression,
 * and factor.
 * 
 * @author Anakaren Borjon
 *
 */


public class ParserTest {

	
	/**
	*This is to unit test the Program
	*method in my parser. 
	*
	*/
	@Test
	public	void testProgram() {
		
		System.out.println("Testing Program: ");
		String test = " begin end .";
		System.out.println("\" "+test + "\" is supposed to have errors");
		Parser parser = new Parser(test, false);
		parser.program();
		
		test = "program l;  begin end .";
		System.out.println("\" "+test + "\" is supposed to  work");
		parser = new Parser(test, false);
		parser.program();
		
		test = "progrm l;  begin end .";
		System.out.println("\" "+test + "\" is supposed to have errors");
		parser = new Parser(test, false);
		parser.program();
		
		test = "program foo ;  begin end .";
		System.out.println("\" "+test + "\" is supposed to  work");
		parser = new Parser(test, false);
		parser.program();
		
	
	}
	
	/**
	*This is to unit test the Declarations
	*method in my parser. 
	*
	*/
	@Test
	
	
	public void testDeclarations() {
		
		System.out.println("-------------------------Testing Declarations() ");
		
		String test = "var merp : integer ; ";
		System.out.println("\" "+test + "\" is supposed to work");
		Parser parser = new Parser(test, false);
		parser.declarations(new DeclarationsNode());
		
		test = "var : integer ; ";
		System.out.println("\" "+test + "\" is supposed to have errors");
		parser = new Parser(test, false);
		parser.declarations(new DeclarationsNode());
		
		test = "var hi  tom ; ";
		System.out.println("\" "+test + "\" is supposed to have errors");
		parser = new Parser(test, false);
		parser.declarations(new DeclarationsNode());
	}
	
	/**
	*This is to unit test the subprogram_declarations
	*method in my parser. 
	*
	*/
	@Test
	
	public void testSubprogram_declarations() {
		
		System.out.println("-------------------------Testing Subprogram_Declarations() ");
		
		String test = "function merp  (hello: integer ) : real ; begin end ;";
		System.out.println("\" "+test + "\" is supposed to work");
		Parser parser = new Parser(test, false);
		parser.subprogram_declaration();
		
		
		test = "procedure merp ; var ma : real ; begin end; ) ; ";
		System.out.println("\" "+test + "\" is supposed to work");
		parser = new Parser(test, false);
		parser.subprogram_declaration();
		
		test = "var : integer ; ";
		System.out.println("\" "+test + "\" is supposed to have errors");
		parser = new Parser(test, false);
		parser.subprogram_declaration();
		

	}
	
	
	/**
	*This is to unit test the simple_expression
	*method in my parser. 
	*
	*/
	@Test
	
	public void testSimpleExpression(){
		System.out.println("-------------------------Testing simple_expression() ");
		
		String test = "merp  / merp ";
		System.out.println("\" "+test + "\" is supposed to work");
		Parser parser = new Parser(test, false);
		parser.simple_expression();
		
		
		test = "merpt [ helo + il  / lo ] +  lil ";
		System.out.println("\" "+test + "\" is supposed to work");
		parser = new Parser(test, false);
		parser.simple_expression();
		
		 test = "[ 52 ] kl m ";
		System.out.println("\" "+test + "\" is supposed to have errors");
		 parser = new Parser(test, false);
		parser.simple_expression();
	}
	
	/**
	*This is to unit test the factor
	*method in my parser. 
	*
	*/
	@Test
	
	
	public void testFactor(){
		System.out.println("-------------------------Testing factor() ");
		
		String test = "merp";
		System.out.println("\" "+test + "\" is supposed to work");
		Parser parser = new Parser(test, false);
		parser.factor();
		
		
		test = "merp [ ji  + ki ] ";
		System.out.println("\" "+test + "\" is supposed to work");
		parser = new Parser(test, false);
		parser.factor();
		
		 test = "[  + ks - v";
		System.out.println("\" "+test + "\" is supposed to have errors");
		 parser = new Parser(test, false);
		parser.factor();
	}
	
	
	
	
	
}
