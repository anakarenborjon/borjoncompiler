package syntaxtree;

public class ReadNode extends StatementNode {
	private VariableNode name;
	
	public ReadNode(VariableNode vN){
		this.name = vN;
	}
	public VariableNode getName(){
		return name;
	}
	public void setName(VariableNode name){
		this.name = name;
	}
	@Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "ReadNode:" + "\n";
        answer += name.indentedToString( level + 1);
        return answer;
    }

}
