package syntaxtree;

/**
 * General representation of any expression.
 * @author erik
 */
public abstract class ExpressionNode extends SyntaxTreeNode {
	public Datatype dT;
	
	public ExpressionNode(){
		this.dT = null;
	}
	public Datatype getDatatype(){
		return dT;
	}
	public void setDatatype(Datatype dT){
		this.dT = dT;
	}
    
}
