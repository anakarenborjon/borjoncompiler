package syntaxtree;

import java.util.ArrayList;
/**
 * represents a set of procedure statements in a pascal file
 * @author Anakaren
 *
 */

public class ProcedureStatementNode extends StatementNode{
	private String name;

	private ArrayList<ExpressionNode> expressions = new ArrayList<ExpressionNode>();


	public ProcedureStatementNode(String name){
		this.name = name;
	}
	/**
	 * Adds a Expression to this Procedure statement.
	 * @param expression The expression node to add to this Procedure statement.
	 */
	public void addExpression(ExpressionNode expression){
		this.expressions.add(expression);

	}
	public String getName(){
		return name; }
	
	public void setName(String name){
		this.name = name;
	}
	public ArrayList<ExpressionNode> getExpression(){
		return expressions;
	}
	

	@Override
	public String indentedToString( int level) {

		String answer = this.indentation( level);
		answer += "Procedure " + getName();
		for( ExpressionNode expression : expressions) {
			answer += expression.indentedToString( level + 1);
		}
		return answer;
	}
	
	
	


}

