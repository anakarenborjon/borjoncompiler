package syntaxtree;
/**
 * represents a set of Arrays in a pascal file
 * @author Anakaren
 *
 */

public class ArrayNode extends VariableNode {
	
	public ExpressionNode expression;
	public ArrayNode(String a){
		super(a);
	}
	public ExpressionNode getExpression(){
		return expression;
	}
	public void setExpression(ExpressionNode expression){
		this.expression = expression;
	}
	public String getName(){
		return super.getName();
	}
	
	
	
	
	
	
	@Override
	    public String indentedToString( int level) {
	        String answer = this.indentation( level);
	        answer += "Array " + this.name;
	        answer += "Expression " + this.getExpression();
	        return answer;
	    }
}
