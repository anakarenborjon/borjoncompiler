package syntaxtree;

/**
 * Represents a While statement in Pascal.
 * A while statement includes a boolean expression, and a statement.
 * @author Anakaren
 */

public class WhileNode extends StatementNode {
	
	    private ExpressionNode test;
	    private StatementNode doStatement;
	    

	    public ExpressionNode getTest() {
	        return test;
	    }

	    public void setTest(ExpressionNode test) {
	        this.test = test;
	    }

	    public StatementNode getDoStatement() {
	        return doStatement;
	    }

	    public void setDoStatement(StatementNode DoStatement) {
	        this.doStatement = doStatement;
	    }

	    
	    /**
	     * Creates a String representation of this node and its children.
	     * @param level The tree level at which this node resides.
	     * @return A String representing this node.
	     */
	    @Override
	    public String indentedToString( int level) {
	        String answer = this.indentation( level);
	        answer += "While\n";
	        answer += this.test.indentedToString( level + 1);
	        answer += this.doStatement.indentedToString( level + 1);
	        
	        return answer;
	    }


}
