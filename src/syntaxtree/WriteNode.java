package syntaxtree;

public class WriteNode extends StatementNode {
	private ExpressionNode name;
	
	public WriteNode(ExpressionNode eN){
		this.name = eN;
	}
	public ExpressionNode getName(){
		return name;
	}
	public void setNAme(VariableNode name){
		this.name = name;
	}
	@Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "WriteNode:" + "\n";
        answer += name.indentedToString( level + 1);
        return answer;
    }

}
