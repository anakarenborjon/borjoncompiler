package syntaxtree;

import static org.junit.Assert.*;

/**
 * jUnit testing for syntax tree
 * @author Anakaren
 * 
 */

import org.junit.Test;

import scanner.TokenType;

public class SyntaxTreeTest {
	
	
	@Test
	public void testIndentedToString() {
		
		/**
		 * ProgramNode
		 */
		ProgramNode pN = new ProgramNode("merp");
		
		/**
		 * DeclarationsNode
		 */
		DeclarationsNode dN = new DeclarationsNode();
		dN.addVariable(new VariableNode("Dollars"));
		dN.addVariable(new VariableNode("Yen"));
		dN.addVariable(new VariableNode("Bitcoins"));
		pN.setVariables(dN);
	
			
		/**
		 * SubProgramDeclarationsNode
		 */
		SubProgramDeclarationsNode spd = new SubProgramDeclarationsNode();
		pN.setFunctions(spd);
		
		/**
		 * CompoundStatementNode
		 */
		
		CompoundStatementNode cS = new CompoundStatementNode();
		AssignmentStatementNode ds = new AssignmentStatementNode();
		ds.setLvalue( new VariableNode("Dollars"));
		ds.setExpression(new ValueNode("1000000"));
		
		
		AssignmentStatementNode yn = new AssignmentStatementNode();
		yn.setLvalue( new VariableNode("Yen"));
		
		OperationNode m = new OperationNode(TokenType.MULTY);
		m.setLeft(new VariableNode("Dollars"));
		m.setRight(new ValueNode("102"));
		yn.setExpression(m);
		
		
		AssignmentStatementNode bc = new AssignmentStatementNode();
		bc.setLvalue(new VariableNode("Bitcoins"));
		OperationNode o = new OperationNode(TokenType.SLASH);
		o.setLeft(new VariableNode("Dollars"));
		o.setRight(new ValueNode("400"));
		bc.setExpression(o);
		
		cS.addStatement(ds);
		cS.addStatement(yn);
		cS.addStatement(bc);
		
		pN.setMain(cS);
		
		String expected = "Program: merp\n" + 
				"|-- Declarations\n" + 
				"|-- --- Name: Dollars\n" + 
				"|-- --- Name: Yen\n" + 
				"|-- --- Name: Bitcoins\n" + 
				"|-- SubProgramDeclarations\n" + 
				"|-- Compound Statement\n" + 
				"|-- --- Assignment\n" + 
				"|-- --- --- Name: Dollars\n" + 
				"|-- --- --- Value: 1000000\n" + 
				"|-- --- Assignment\n" + 
				"|-- --- --- Name: Yen\n" + 
				"|-- --- --- Operation: MULTY\n" + 
				"|-- --- --- --- Name: Dollars\n" + 
				"|-- --- --- --- Value: 102\n" + 
				"|-- --- Assignment\n" + 
				"|-- --- --- Name: Bitcoins\n" + 
				"|-- --- --- Operation: SLASH\n" + 
				"|-- --- --- --- Name: Dollars\n" + 
				"|-- --- --- --- Value: 400\n";
		
		String returned = pN.indentedToString(0);
		assertEquals(expected, returned);
		
		
		
		
		
	}
}


