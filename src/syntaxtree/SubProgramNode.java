package syntaxtree;
/**
 * Represents a Pascal SubProgram Node
 * @author Anakaren
 */

public class SubProgramNode extends ProgramNode {
	
	private Datatype dT;
	private SubpT type;
	private ParameterNode parameter;
	
	public ParameterNode getParameter(){
		return parameter;
	}
	public SubpT getType(){
		return type;
	}
	public void setParameter(ParameterNode parameter){
		this.parameter = parameter;
	}
	public void setType(SubpT type){
		this.type = type;
	}
	public Datatype getDatatype(){
		return dT;
	}
	public void setDatatype(){
		this.dT = dT;
	}
	
	public SubProgramNode(String aName){
		super(aName);
		dT = null;
		setVariables(new DeclarationsNode());
		setFunctions(new SubProgramDeclarationsNode());
		setMain(new CompoundStatementNode());
	}
	/**
     * Creates a String representation of this SubProgram node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
	@Override
	public String indentedToString(int level){
		String answer = this.indentation(level);
		answer +="SubProgram " + getName();
		answer += this.getParameter().indentation(level + 1 );
		answer += this.getVariables().indentation(level + 1);
		answer += this.getFunctions().indentation(level + 1);
		answer += this.getMain().indentation(level + 1);
		
		
		return answer;
	}
	
}